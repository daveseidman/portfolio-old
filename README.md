# Portfolio Website

WebGL background with carousel of recent work.




### TODO:
- webkit tap highlight (look on google pixel)
- Reset videos on open project
- www subdomain not redirecting to https
- Add contact info?
- Try Camera Jerk
- Incorporate comments
- add motion blur
- add soundtrack?
- re-record wedding website
- Add ASCO, CES, DLS, Spotify Codes, Motion Graphics
- add shadows?
- try transmission shader?
- move particles into class
- still able to open projects while scrolling away from them (mostly touch)
- try npm detect-gpu for adjusting pixel ratio
- remove carousel callout
- test video playback on slower devices, consider turning down pixel ratio while projects are open
- fuzzy search on keypress, i.e. typing "C", "A", would jump to catdive or cam-repeater (whichever is closer)
- diagnose horizontal resize issue with carousel (bouncing);
- The "Elon" button... Click it, authenticate with Twitter API, if handle === @elonmusk, do something, else, say "thanks, but waiting for elon to click it, do you want to be notified when he does?"
